When building stellar-core, its use of submodules makes life a little difficult. You would normally need to use the stellar-core build with a particular tag and then git would do its magic and pull in the correct commits from the submodules at the date of the tagged build. For this build, you will need to download these submodules yourself as we need the sources at the srpm stage and not during the build. The easiest way to do this is to clone the stellar-core repo with the correct tag, go into the repo and initialise the submodules. Do something like:
```
git clone --depth=1 --branch {tag-name} https://github.com/stellar/stellar-core.git stellar-core-git
cd stellar-core-git
git submodule update --init --recursive
git submodule
```
The output of the last command will show you the commits of all the submodules used. This information can by used to populate the `sources.download` with the files to download. Ignore the libsodium submodule as this is pulled in as a dependency rather than statically linked and instead there is a Requires and BuildRequires for it.

You can remove the stellar-core-git folder afterwards

If the package list changes in `.gitmodules` then the spec file and sources.download will need updating.
