Summary: Stellar Core with sqlite3 support only
URL: https://www.stellar.org
Name: stellar-core
Version: 19.3.0
Release: 1%{?dist}
BuildRoot: %{_builddir}/%{name}-root
BuildArch: x86_64
License: Apache License 2.0

Requires: libsodium
Requires: libunwind

BuildRequires: automake
BuildRequires: bison
BuildRequires: devtoolset-8
BuildRequires: devtoolset-8-build
BuildRequires: flex
BuildRequires: git
BuildRequires: libsodium-devel
BuildRequires: libtool
BuildRequires: libunwind-devel
BuildRequires: pandoc

Source0: v%{version}.tar.gz
Source1: asio.tar.gz
Source2: cereal.tar.gz
Source3: fmt.tar.gz
Source4: libmedida.tar.gz
#Source5: libsodium.tar.gz
Source6: spdlog.tar.gz
Source7: tracy.tar.gz
Source8: xdrpp.tar.gz
source9: xdr.tar.gz
Source100: stellar-core.service
Source101: stellar.cfg

%description
stellar-core is the reference implementation for the peer to peer agent that manages the Stellar network

%prep
%autosetup -n %{name}-%{version} -S git
tar xvf %{_sourcedir}/asio.tar.gz      --strip-components=1 -C %{_builddir}/%{name}-%{version}/lib/asio
tar xvf %{_sourcedir}/cereal.tar.gz    --strip-components=1 -C %{_builddir}/%{name}-%{version}/lib/cereal
tar xvf %{_sourcedir}/fmt.tar.gz       --strip-components=1 -C %{_builddir}/%{name}-%{version}/lib/fmt
tar xvf %{_sourcedir}/libmedida.tar.gz --strip-components=1 -C %{_builddir}/%{name}-%{version}/lib/libmedida
#tar xvf %{_sourcedir}/libsodium.tar.gz --strip-components=1 -C %{_builddir}/%{name}-%{version}/lib/libsodium
tar xvf %{_sourcedir}/spdlog.tar.gz    --strip-components=1 -C %{_builddir}/%{name}-%{version}/lib/spdlog
tar xvf %{_sourcedir}/tracy.tar.gz     --strip-components=1 -C %{_builddir}/%{name}-%{version}/lib/tracy
tar xvf %{_sourcedir}/xdrpp.tar.gz     --strip-components=1 -C %{_builddir}/%{name}-%{version}/lib/xdrpp
tar xvf %{_sourcedir}/xdr.tar.gz       --strip-components=1 -C %{_builddir}/%{name}-%{version}/src/protocol-next/xdr

%build


%{?scl:scl enable %scl - << \EOFF}
# this is a devtoolset-8 enabled shell to give g++-8

export CXX=g++
./autogen.sh
%{configure} --disable-postgres --docdir=/usr/share/doc/%{name}-%{version}
make %{?_smp_mflags}

# End of devtoolset-8 enabled section
%{?scl:EOFF}

%install
export DESTDIR=%{buildroot}
make install

install -D -m0644 %{SOURCE100} %{buildroot}%{_unitdir}/%{name}.service
install -D -m0644 %{SOURCE101} %{buildroot}%{_sysconfdir}/%{name}/stellar.cfg
mkdir -p %{buildroot}/var/lib/stellar-core/history/vs/.well-known/

%post
if [ $1 == 1 ] ; then
    # initialise stellar-core database
    %{_bindir}/%{name} --conf %{_sysconfdir}/%{name}/stellar.cfg new-db > /dev/null 2>&1
fi
systemctl daemon-reload
%systemd_post stellar-core.service

%preun
%systemd_preun stellar-core.service

%postun
%systemd_postun_with_restart stellar-core.service

%files
%defattr(-,root,root)
%attr(0755, -, -) %{_bindir}/%{name}
/var/lib/stellar-core
%config(noreplace) %{_sysconfdir}/%{name}/stellar.cfg
%{_unitdir}/%{name}.service
%{_docdir}/%{name}-%{version}

%changelog
* Wed Aug 03 2022 Nick Howitt <support@clearcenter.com> 19.3.0-1
- Upstream to v19.3.0

* Thu Jun 30 2022 Nick Howitt <support@clearcenter.com> 19.2.0-2
- Add runtime requirement of libunwind

* Wed Jun 29 2022 Nick Howitt <support@clearcenter.com> 19.2.0-1
- Upstream to v19.2.0
- Switch build to g++-v8 from devtoolset-8
- Remove sqlite3 dependency (and put it in app-digitalworld)

* Thu Jun  2 2022 Nick Howitt <support@clearcenter.com>
- Upstream to v19.1.0

* Tue May 10 2022 Nick Howitt <support@clearcenter.com>
- Upstream to v19.0.1

* Wed Apr 27 2022 Nick Howitt <support@clearcenter.com>
- Upstream to v19.0.0

* Tue Apr 19 2022 Nick Howitt <support@clearcenter.com>
- Upstream to v18.5.0

* Tue Jan 25 2022 Nick Howitt <support@clearcenter.com>
- Upstream to v18.3.0

* Mon Dec 13 2021 Nick Howitt <support@clearcenter.com>
- Upstream to v18.2.0

* Tue Oct 26 2021 Nick Howitt <support@clearcenter.com>
- Upstream to v18.1.0

* Mon Oct 11 2021 Nick Howitt <support@clearcenter.com>
- Upstream to v18.0.3
- Add BuildRequires of devtoolset-8 for koji
- Add multi-core build support

* Wed Oct  6 2021 Nick Howitt <support@clearcenter.com>
- Upstream to v18.0.1

* Wed May 26 2021 Nick Howitt <support@clearcenter.com>
- Upstream to v17.1.0

* Thu Apr 29 2021 Nick Howitt <support@clearcenter.com>
- Upstream to v17.0.0

* Mon Apr 12 2021 Nick Howitt <support@clearcenter.com>
- Run stellar-core as root instead of stellar

* Fri Apr 9 2021 Nick Howitt <support@clearcenter.com>
- Upstream to v16.0.0

* Thu Apr 8 2021 Nick Howitt <support@clearcenter.com>
- Upstream to v15.5.0

* Thu Apr 1 2021 Nick Howitt <support@clearcenter.com>
- Upstream to v15.4.0

* Wed Mar 24 2021 Nick Howitt <support@clearcenter.com>
- Flatten repo
- remove "git submodule init" from the build stage and download all sources when creating the srpm

* Thu Mar 11 2021 Nick Howitt <support@clearcenter.com>
- Fixups

* Thu Mar 11 2021 Nick Howitt <support@clearcenter.com>
- Upstream to 15.3.0-1
